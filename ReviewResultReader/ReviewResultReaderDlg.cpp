﻿
// ReviewResultReaderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ReviewResultReader.h"
#include "ReviewResultReaderDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define CTC 255
#define CTC2 16711935
#define NONCTC 16776960

// CReviewResultReaderDlg dialog



CReviewResultReaderDlg::CReviewResultReaderDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CReviewResultReaderDlg::IDD, pParent)
	, m_NumReviewers(3)
	, m_RgnFilename(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CReviewResultReaderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_NUMREVIEWERS, m_NumReviewers);
	DDX_Text(pDX, IDC_RGNFILENAME, m_RgnFilename);
	DDX_Control(pDX, IDC_RESULTLIST, m_ResultList);
	DDX_Control(pDX, IDC_AT3, m_AT3);
	DDX_Control(pDX, IDC_AT4, m_AT4);
}

BEGIN_MESSAGE_MAP(CReviewResultReaderDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_OPENRGN, &CReviewResultReaderDlg::OnBnClickedOpenrgn)
	ON_BN_CLICKED(IDCANCEL, &CReviewResultReaderDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CReviewResultReaderDlg message handlers

BOOL CReviewResultReaderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	int nSize[] = { 100, 80, 80, 80, 80, 500 };
	LV_COLUMN nListColumn;
	for (int i = 0; i < 6; i++)
	{
		nListColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		nListColumn.fmt = LVCFMT_LEFT;
		nListColumn.cx = nSize[i];
		nListColumn.iSubItem = 0;
		if (i == 0)
			nListColumn.pszText = _T("Reviewer");
		else if (i == 1)
			nListColumn.pszText = _T("#Regions");
		else if (i == 2)
			nListColumn.pszText = _T("#PbleCTCs");
		else if (i == 3)
			nListColumn.pszText = _T("#CfrmCTCs");
		else if (i == 4)
			nListColumn.pszText = _T("#NonCTCs");
		else if (i == 5)
			nListColumn.pszText = _T("#CheckedReviewCriteria");

		m_ResultList.InsertColumn(i, &nListColumn);
	}
	m_ResultList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_AT3.SetCheck(BST_CHECKED);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CReviewResultReaderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CReviewResultReaderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CReviewResultReaderDlg::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN))
	{
		UpdateData(TRUE);
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}


void CReviewResultReaderDlg::OnBnClickedOpenrgn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CleanUp();
	int itemIndex = 0;

	bool failed = false;
	for (int i = 0; i < m_NumReviewers; i++)
	{
		CString nameStr;
		nameStr.Format(_T("ReviewerNo.%d RGN File (*.rgn)|*.rgn"), i + 1);
		CFileDialog dlg(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			nameStr, NULL, 0, TRUE);
		if (dlg.DoModal() == IDOK)
		{
			CString filename = dlg.GetPathName();
			WCHAR *char1 = _T("\\");
			int index = filename.ReverseFind(*char1);
			CString safeFilename = filename.Mid(index+1);
			index = safeFilename.Find(_T(".rgn"));
			safeFilename = safeFilename.Mid(0, index);
			m_RgnFilename += safeFilename + _T("; ");
			CString reviewerName = _T("");
			CReviewDataList *list = NULL;
			int numCTC1 = 0;
			int numCTC2 = 0;
			int numNCTC = 0;
			int *counts = NULL;
			if (!LoadReviewData(filename, &reviewerName, &list, &numCTC1, &numCTC2, &numNCTC, &counts))
			{
				CString message;
				message.Format(_T("Failed to load Review Data from %s"), safeFilename);
				AfxMessageBox(message);
				failed = true;
				return;
			}
			if (list->m_List->size() > 0)
			{
				m_ResultData.push_back(list);
				CString labelStr;
				labelStr.Format(_T("%s"), reviewerName);
				int idx = m_ResultList.InsertItem(itemIndex++, labelStr);
				labelStr.Format(_T("%d"), list->m_List->size());
				m_ResultList.SetItemText(idx, 1, labelStr);
				labelStr.Format(_T("%d"), numCTC1);
				m_ResultList.SetItemText(idx, 2, labelStr);
				labelStr.Format(_T("%d"), numCTC2);
				m_ResultList.SetItemText(idx, 3, labelStr);
				labelStr.Format(_T("%d"), numNCTC);
				m_ResultList.SetItemText(idx, 4, labelStr);
				labelStr = _T("");
				CString textStr;
				for (int i = 0; i < (*list->m_List)[0]->m_ReviewAnswerLength; i++)
				{
					textStr.Format(_T("%d, "), counts[i]);
					labelStr += textStr;
				}
				m_ResultList.SetItemText(idx, 5, labelStr);
			}
			else
			{
				CString message;
				message.Format(_T("#Region=0 in %s"), safeFilename);
				AfxMessageBox(message);
				failed = true;
			}
			if (counts != NULL)
				delete counts;
			if (failed)
			{
				return;
			}
			else
			{
				UpdateData(FALSE);
			}
		}
		else
		{
			CString message;
			message.Format(_T("Failed to select a Region Result File for Reviewer No.%d"), i+1);
			AfxMessageBox(message);
			failed = true;
			return;
		}
	}
	if (!failed)
	{
		if (m_ResultData.size() >= 3)
		{
			GetConsensus();
			UpdateData(FALSE);
		}
		else
		{
			CString message;
			message.Format(_T("#Reviewers=%u, Failed to get consensus."), m_ResultData.size());
			AfxMessageBox(message);
		}
	}
}

void CReviewResultReaderDlg::GetConsensus()
{
	int ctc = 0;
	int majority = (int)m_ResultData.size() / 2;
	if (((int)m_ResultData.size() % (majority * 2)) == 1)
		majority++;

	int *reviewerCount = new int[(int)m_ResultData.size()];
	memset(reviewerCount, 0, sizeof(int)*((int)m_ResultData.size()));

	for (int rgnIdx = 0; rgnIdx < (int)m_ResultData[0]->m_List->size(); rgnIdx++)
	{
		int numCTC = 0;
		int x0 = (*m_ResultData[0]->m_List)[rgnIdx]->m_X0;
		int y0 = (*m_ResultData[0]->m_List)[rgnIdx]->m_Y0;
		for (int i = 0; i < (int)m_ResultData.size(); i++)
		{
			int x1 = (*m_ResultData[i]->m_List)[rgnIdx]->m_X0;
			int y1 = (*m_ResultData[i]->m_List)[rgnIdx]->m_Y0;
			if ((x0 != x1) || (y0 != y1))
			{
				CString message; 
				message.Format(_T("Region %d Positions are not matched between ReviewerNo.1(%d,%d) and ReviewerNo.%d(%d,%d)"),
					rgnIdx, x0, y0, i + 1, x1, y1);
				AfxMessageBox(message);
				return;
			}
			unsigned int color = (*m_ResultData[i]->m_List)[rgnIdx]->m_ColorCode;
			if ((color == CTC) || (color == CTC2))
			{
				reviewerCount[i]++;
				numCTC++;
			}
		}
		if (numCTC >= majority)
			ctc++;
	}
	CString labelStr;
	labelStr = _T("Consensus");
	int idx = m_ResultList.InsertItem((int)m_ResultData.size(), labelStr);
	labelStr.Format(_T("%d"), ctc);
	m_ResultList.SetItemText(idx, 3, labelStr);
	CStdioFile theFile;
	int index = m_RgnFilename.Find(_T("_Reviewer"));
	if (index == -1)
	{
		CString message;
		message.Format(_T("Failed to get sample name from %s"), m_RgnFilename);
		AfxMessageBox(message);
		return;
	}
	else
	{
		CString consensusResult;
		consensusResult.Format(_T("_Consenses_%d"), ctc);
		for (int i = 0; i < (int)m_ResultData.size(); i++)
		{
			CString substr;
			substr.Format(_T("_%d"), reviewerCount[i]);
			consensusResult += substr;
		}
		consensusResult += _T(".csv");
		CString filename = _T(".\\") + m_RgnFilename.Mid(0, index) + consensusResult;
		if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			CString textline;
			textline.Format(_T("#CTCS=%d\n"), ctc);
			theFile.WriteString(textline);
			textline.Format(_T("Review Region Files=%s\n"), m_RgnFilename);
			theFile.WriteString(textline);
			textline.Format(_T("RgnIdx,X0,Y0,"));
			for (int i = 0; i < (int)m_ResultData.size(); i++)
			{
				CString subStr;
				subStr.Format(_T("Color%d,"), i + 1);
				textline += subStr;
			}
			for (int rIdx = 0; rIdx < (int)m_ResultData.size(); rIdx++)
			{
				for (int i = 0; i < (*m_ResultData[0]->m_List)[0]->m_ReviewAnswerLength; i++)
				{
					CString subStr;
					subStr.Format(_T("Attr(%d)%d,"), rIdx + 1, i + 1);
					textline += subStr;
				}
			}
			textline += _T("\n");
			theFile.WriteString(textline);
			for (int i = 0; i < (int)m_ResultData[0]->m_List->size(); i++)
			{
				textline.Format(_T("%d,%d,%d,%s,"), i + 1, (*m_ResultData[0]->m_List)[i]->m_X0, (*m_ResultData[0]->m_List)[i]->m_Y0,
					(((*m_ResultData[0]->m_List)[i]->m_ColorCode == CTC) ? _T("P") : ((*m_ResultData[0]->m_List)[i]->m_ColorCode == CTC2) ? _T("C") : _T("N")));
				for (int j = 1; j < (int)m_ResultData.size(); j++)
				{
					CString subStr;
					subStr.Format(_T("%s,"), (((*m_ResultData[j]->m_List)[i]->m_ColorCode == CTC) ? _T("P") : ((*m_ResultData[j]->m_List)[i]->m_ColorCode == CTC2) ? _T("C") : _T("N")));
					textline += subStr;
				}
				for (int rIdx = 0; rIdx < (int)m_ResultData.size(); rIdx++)
				{
					for (int j = 0; j < (*m_ResultData[0]->m_List)[0]->m_ReviewAnswerLength; j++)
					{
						CString subStr;
						subStr.Format(_T("%d,"), ((*m_ResultData[rIdx]->m_List)[i]->m_ReviewAnswer[j]?1:0));
						textline += subStr;
					}
				}
				textline += _T("\n");
				theFile.WriteString(textline);
			}
			
			textline.Format(_T(",,Total"));
			for (int i = 0; i < (int)m_ResultData.size(); i++)
			{
				CString substr;
				substr.Format(_T(",%d"), reviewerCount[i]);
				textline += substr;
			}
			textline += _T("\n");
			theFile.WriteString(textline);
			delete reviewerCount;
			theFile.Close();
		}
		else
		{
			CString message;
			message.Format(_T("Failed to open file %s"), filename);
			AfxMessageBox(message);
			return;
		}
	}
}

bool CReviewResultReaderDlg::LoadReviewData(CString filename, CString *reviewer, CReviewDataList **list, int *pbleCTCs, int *cfrmCTCs, int *nonCTCs, int **attrCounts)
{
	const CString TAG1 = _T("0 1, 1 ");
	const CString TAG2 = _T(", 2 ");
	const CString TAG3 = _T(", ");

	bool ret = false;
	int ctc1 = 0;
	int ctc2 = 0;
	int nonCTC = 0;

	CString extension = _T(".at3");
	int numAttributes = 9;
	if (m_AT4.GetCheck() == BST_CHECKED)
	{
		extension = _T(".at4");
		numAttributes = 4;
	}
	int index = filename.Find(_T(".rgn"));
	CString archiveFilename = filename.Mid(0, index);
	archiveFilename += extension;
	CStdioFile theFile;
	CFile archiveFile;
	if (!archiveFile.Open(archiveFilename, CFile::modeRead))
	{
		CString message;
		message.Format(_T("Failed to open Review Data File %s"), archiveFilename);
		return ret;
	}
	if (theFile.Open(filename, CFile::modeRead | CFile::typeText))
	{
		CReviewDataList *dataList = new CReviewDataList();
		*list = dataList;
		int *counts = new int[numAttributes];
		*attrCounts = counts;
		memset(counts, 0, sizeof(int) * numAttributes);
		CArchive archive(&archiveFile, CArchive::load);
		CString reviewerName = _T("");
		archive >> reviewerName;
		*reviewer = reviewerName;
		CString textline;
		bool failed = false;
		while (theFile.ReadString(textline))
		{
			int index = textline.Find(TAG1);
			if (index > -1)
			{
				textline = textline.Mid(TAG1.GetLength());
				index = textline.Find(TAG2);
				if (index > -1)
				{
					unsigned int color = _wtoi(textline.Mid(0, index));
					textline = textline.Mid(index + TAG2.GetLength());
					index = textline.Find(_T(" "));
					if (index > -1)
					{
						int x0 = _wtoi(textline.Mid(0, index));
						textline = textline.Mid(index + 1);
						index = textline.Find(TAG3);
						if (index > -1)
						{
							int y0 = _wtoi(textline.Mid(0, index));
							CReviewData *rgn = new CReviewData(numAttributes);
							rgn->m_X0 = x0;
							rgn->m_Y0 = y0;
							rgn->m_ColorCode = color;
							if (color == CTC)
								ctc1++;
							else if (color == CTC2)
								ctc2++;
							else
								nonCTC++;
							try
							{
								archive.Read(rgn->m_ReviewAnswer, sizeof(bool) * rgn->m_ReviewAnswerLength);
								for (int i = 0; i < rgn->m_ReviewAnswerLength; i++)
								{
									if (rgn->m_ReviewAnswer[i])
										counts[i]++;
								}
							}
							catch (...)
							{
								CString message;
								message.Format(_T("Failed to read Attribute Data from %s"), archiveFilename);
								failed = true;
								break;
							}
							dataList->m_List->push_back(rgn);
							continue;
						}
						else
						{
							break;
						}
					}
					else
					{
						break;
					}
				}
				else
				{
					break;
				}
			}
			else
			{
				break;
			}
		}
		if (!failed)
		{
			*pbleCTCs = ctc1;
			*cfrmCTCs = ctc2;
			*nonCTCs = nonCTC;
			ret = true;
		}
		else
		{
			delete counts;
			*attrCounts = NULL;
			delete *list;
			*list = NULL;
		}
		theFile.Close();
		archive.Close();
		archiveFile.Close();
	}
	return ret;
}

void CReviewResultReaderDlg::CleanUp()
{
	m_ResultList.DeleteAllItems();
	m_RgnFilename = _T("");
	UpdateData(FALSE);
	for (int i = 0; i < (int)m_ResultData.size(); i++)
	{
		delete m_ResultData[i];
		m_ResultData[i] = NULL;
	}
	m_ResultData.clear();
}

void CReviewResultReaderDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	for (int i = 0; i < (int)m_ResultData.size(); i++)
	{
		delete m_ResultData[i];
		m_ResultData[i] = NULL;
	}
	m_ResultData.clear();
	CDialogEx::OnCancel();
}
