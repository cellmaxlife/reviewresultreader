//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 產生的 Include 檔案。
// 由 ReviewResultReader.rc 使用
//
#define IDD_REVIEWRESULTREADER_DIALOG   102
#define IDR_MAINFRAME                   128
#define IDC_NUMREVIEWERS                1000
#define IDC_OPENRGN                     1001
#define IDC_RGNFILENAME                 1002
#define IDC_LIST1                       1003
#define IDC_RESULTLIST                  1003
#define IDC_REVIEWTYPE                  1004
#define IDC_AT3                         1005
#define IDC_AT4                         1006
#define IDC_RGNFILENAME2                1007
#define IDC_NUMCTCS                     1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
