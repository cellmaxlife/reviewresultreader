
// ReviewResultReaderDlg.h : header file
//

#pragma once
#include "ReviewDataList.h"
#include <vector>
#include "afxcmn.h"
#include "afxwin.h"

using namespace std;

// CReviewResultReaderDlg dialog
class CReviewResultReaderDlg : public CDialogEx
{
// Construction
public:
	CReviewResultReaderDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_REVIEWRESULTREADER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	BOOL PreTranslateMessage(MSG* pMsg);
	int m_NumReviewers;
	CString m_RgnFilename;
	afx_msg void OnBnClickedOpenrgn();
	afx_msg void OnBnClickedCancel();
	vector<CReviewDataList *> m_ResultData;
	CListCtrl m_ResultList;
	void CleanUp();
	bool LoadReviewData(CString filename, CString *reviewer, CReviewDataList **list, int *pbleCTCs, int *cfrmCTCs, int *nonCTCs, int **attrCounts);
	CButton m_AT3;
	CButton m_AT4;
	void GetConsensus();
};
