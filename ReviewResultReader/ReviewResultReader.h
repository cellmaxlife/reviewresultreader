
// ReviewResultReader.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CReviewResultReaderApp:
// See ReviewResultReader.cpp for the implementation of this class
//

class CReviewResultReaderApp : public CWinApp
{
public:
	CReviewResultReaderApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CReviewResultReaderApp theApp;