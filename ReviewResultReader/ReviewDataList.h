#pragma once

#include <vector>
#include "ReviewData.h"

using namespace std;

class CReviewDataList
{
public:
	CReviewDataList();
	~CReviewDataList();
	vector<CReviewData *> *m_List;
};