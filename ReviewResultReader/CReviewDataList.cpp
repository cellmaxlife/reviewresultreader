#include "stdafx.h"
#include "ReviewDataList.h"

CReviewDataList::CReviewDataList()
{
	m_List = new vector<CReviewData *>();
}

CReviewDataList::~CReviewDataList()
{
	for (int i = 0; i < (int)m_List->size(); i++)
	{
		delete (*m_List)[i];
		(*m_List)[i] = NULL;
	}
	m_List->clear();
	delete m_List;
}