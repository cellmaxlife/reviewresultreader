#pragma once

class CReviewData
{
public:
	CReviewData(int numAttributes);
	~CReviewData();

	int m_X0;
	int m_Y0;
	unsigned int m_ColorCode;
	int m_ReviewAnswerLength;
	bool *m_ReviewAnswer;
};