#include "stdafx.h"
#include "ReviewData.h"

CReviewData::CReviewData(int numAttributes)
{
	m_ReviewAnswerLength = numAttributes;
	m_ReviewAnswer = new bool[m_ReviewAnswerLength];
}

CReviewData::~CReviewData()
{
	delete m_ReviewAnswer;
}